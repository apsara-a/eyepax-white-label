<?php

/**
 * The settings of the plugin.
 *
 * @link       http://devinvinson.com
 * @since      1.0.0
 *
 * @package    Eyepax_White_Label
 * @subpackage Eyepax_White_Label/admin
 */

/**
 * Class WordPress_Plugin_Template_Settings
 *
 */
class Eyepax_White_Label_Admin_Settings {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * This function introduces the theme options into the 'Settings' menu and into a top-level
	 * 'Eyepax_White_Label' menu.
	 */
	public function Setup_Plugin_Options_Menu() {

		//Add the menu to the Plugins set of menu items
		add_options_page(
			'Eyepax Options',// The title to be displayed in the browser window for this page.
			'Eyepax Options',// The text to be displayed for this menu item
			'manage_options',// Which type of users can see this menu item
			'eyepax_options',// The unique ID - that is, the slug - for this menu item
			array( $this, 'Render_Settings_Page_content')// The name of the function to call when rendering this menu's page
		);

	}

	/**
	 * Provides default values for the Display Options.
	 *
	 * @return array
	 */
	public function Default_Display_options() {

		$defaults = array(
			'login_username'	=>	'',
			'hide_lostpassword'	=>	'',
			'back_to_blog'		=>	'',
			'login_logo'		=>	'',
			'custom_err_msg_txt'=>	'Please enter valid login credentials.',
		);

		return $defaults;

	}


	/**
	 * Renders a simple page to display for the theme menu defined above.
	 */
	public function Render_Settings_Page_content( $active_tab = '' ) {
		?>
		<!-- Create a header in the default WordPress 'wrap' container -->
		<div class="wrap">

			<h2><?php _e( 'Eyepax While Label Options', 'eyepax-white-label' ); ?></h2>
			<?php //settings_errors(); ?>

			<?php if ( isset( $_GET[ 'tab' ] ) ) {
				$active_tab = $_GET[ 'tab' ];
			} else if ( $active_tab == 'social_options' ) {
				$active_tab = 'social_options';
			} else if ( $active_tab == 'input_examples' ) {
				$active_tab = 'input_examples';
			} else {
				$active_tab = 'display_options';
			} // end if  /else ?>

			<h2 class="nav-tab-wrapper">
				<a href="?page=eyepax_options&tab=display_options" class="nav-tab <?php echo $active_tab == 'display_options' ? 'nav-tab-active' : ''; ?>"><?php _e( 'Display Options', 'eyepax-white-label' ); ?></a>
				<!-- <a href="?page=eyepax_options&tab=social_options" class="nav-tab <?php echo $active_tab == 'social_options' ? 'nav-tab-active' : ''; ?>"><?php _e( 'Social Options', 'eyepax-white-label' ); ?></a> -->
				<!-- <a href="?page=eyepax_options&tab=input_examples" class="nav-tab <?php echo $active_tab == 'input_examples' ? 'nav-tab-active' : ''; ?>"><?php _e( 'Input Examples', 'eyepax-white-label' ); ?></a> -->
			</h2>

			<form method="post" action="options.php">
				<?php

				if ( $active_tab == 'display_options' ) {

					settings_fields( 'ewl_display_options' );
					do_settings_sections( 'ewl_display_options' );

				} elseif ( $active_tab == 'social_options' ) {

					settings_fields( 'eyepax_social_options' );
					do_settings_sections( 'eyepax_social_options' );

				} else {

					settings_fields( 'eyepax__input_examples' );
					do_settings_sections( 'eyepax_input_examples' );

				} // end if  /else

				submit_button();

				?>
			</form>

		</div><!-- /.wrap -->
		<?php
	}


	/**
	 * This function provides a simple description for the General Options page.
	 *
	 * It's called from the 'ewl_initialize_theme_options' function by being passed as a parameter
	 * in the add_settings_section function.
	 */
	public function general_options_callback() {
		$options = get_option('ewl_display_options');
		var_dump($options);
		// echo '<p>' . __( 'Select which areas of content you wish to display.', 'eyepax-white-label' ) . '</p>';
	} // end general_options_callback




	/**
	 * Initializes the theme's display options page by registering the Sections,
	 * Fields, and Settings.
	 *
	 * This function is registered with the 'admin_init' hook.
	 */
	public function initialize_display_options() {

		// if   the theme options don't exist, create them.
		if ( false == get_option( 'ewl_display_options' ) ) {
			$default_array = $this->Default_Display_options();
			add_option( 'ewl_display_options', $default_array );
		}


		add_settings_section(
			'general_settings_section', // ID used to identif  y this section and with which to register options
			__( 'Display Options', 'eyepax-white-label' ),// Title to be displayed on the administration page
			array( $this, 'general_options_callback'),// Callback used to render the description of the section
			'ewl_display_options'// Page on which to add this section of options
		);

		add_settings_field(
			'login_logo',
			__( 'Custom Logo', 'eyepax-white-label' ),
			array( $this, 'toggle_login_logo_callback'),
			'ewl_display_options',
			'general_settings_section',
			array(
				__( 'Activate this setting to show the custom logo. Add logo in <a href="customize.php">here.</a> Logo size 300x100px', 'eyepax-white-label' ),
			)
		);

		// Next, we'll introduce the fields for toggling the visibility of content elements.
		add_settings_field(
			'login_username', // ID used to identif  y the field throughout the theme
			__( 'Login with Username', 'eyepax-white-label' ),// The label to the left of the option interface element
			array( $this, 'Login_With_Username_callback'),// The name of the function responsible for rendering the option interface
			'ewl_display_options',// The page on which this option will be displayed
			'general_settings_section',// The name of the section to which this field belongs
			array( // The array of arguments to pass to the callback. In this case, just a description.
				__( 'Activate login with only username.', 'eyepax-white-label' ),
			)
		);

		add_settings_field(
			'show_content',
			__( 'Lost password', 'eyepax-white-label' ),
			array( $this, 'toggle_content_callback'),
			'ewl_display_options',
			'general_settings_section',
			array(
				__( 'Activate this setting to hide the lost password.', 'eyepax-white-label' ),
			)
		);

		add_settings_field(
			'back_to_blog',
			__( 'Back to blog', 'eyepax-white-label' ),
			array( $this, 'toggle_footer_callback'),
			'ewl_display_options',
			'general_settings_section',
			array(
				__( 'Activate this setting to hide the back to blog.', 'eyepax-white-label' ),
			)
		);

		add_settings_field(
			'custom_error_msg',
			__( 'Custom error message', 'eyepax-white-label' ),
			array( $this, 'toggle_custom_error_msg_callback'),
			'ewl_display_options',
			'general_settings_section',
			array(
				__( 'Activate this setting to hide the back to blog.', 'eyepax-white-label' ),
			)
		);



		// Finally, we register the fields with WordPress
		register_setting(
			'ewl_display_options',
			'ewl_display_options'
		);

	} // end ewl_initialize_theme_options




	/**
	 * This function renders the interface elements for toggling the visibility of the header element.
	 *
	 * It accepts an array or arguments and expects the first element in the array to be the description
	 * to be displayed next to the checkbox.
	 */
	public function Login_With_Username_callback($args) {

		// First, we read the options collection
		$options = get_option('ewl_display_options');

		// Next, we update the name attribute to access this element's ID in the context of the display options array
		// We also access the login_username element of the options collection in the call to the checked() helper function
		$html = '<input type="checkbox" id="login_username" name="ewl_display_options[login_username]" value="1" ' . checked( 1, isset( $options['login_username'] ) ? $options['login_username'] : 0, false ) . '/>';

		// Here, we'll take the first argument of the array and add it to a label next to the checkbox
		$html .= '<label for="login_username">&nbsp;'  . $args[0] . '</label>';

		echo $html;

	} // end Login_With_Username_callback

	public function toggle_content_callback($args) {

		$options = get_option('ewl_display_options');

		$html = '<input type="checkbox" id="hide_lostpassword" name="ewl_display_options[hide_lostpassword]" value="1" ' . checked( 1, isset( $options['hide_lostpassword'] ) ? $options['hide_lostpassword'] : 0, false ) . '/>';
		$html .= '<label for="hide_lostpassword">&nbsp;'  . $args[0] . '</label>';

		echo $html;

	} // end toggle_content_callback

	public function toggle_footer_callback($args) {

		$options = get_option('ewl_display_options');

		$html = '<input type="checkbox" id="back_to_blog" name="ewl_display_options[back_to_blog]" value="1" ' . checked( 1, isset( $options['back_to_blog'] ) ? $options['back_to_blog'] : 0, false ) . '/>';
		$html .= '<label for="back_to_blog">&nbsp;'  . $args[0] . '</label>';

		echo $html;

	} // end toggle_footer_callback	

	public function toggle_custom_error_msg_callback($args) {

		$options = get_option('ewl_display_options');

		$html = '<input type="checkbox" id="custom_err_msg" name="ewl_display_options[custom_err_msg]" value="1" ' . checked( 1, isset( $options['custom_err_msg'] ) ? $options['custom_err_msg'] : 0, false ) . 'onclick="OnChangeCheckbox (this)" />';
		$html .= '<label for="custom_err_msg">&nbsp;'  . $args[0] . '</label>';
		$display = ($options['custom_err_msg'] == true) ? "display:block" : "display:none";
		$html .= '<p id="custom_txt" style="'. $display .'"><input type="text"  id="custom_err_msg_txt" name="ewl_display_options[custom_err_msg_txt]" value="' . $options['custom_err_msg_txt'] . '" /></p>';

		echo $html;

	} // end toggle_custom_error_msg_callback	

	public function toggle_login_logo_callback($args) {

		$options = get_option('ewl_display_options');

		add_theme_support( 'custom-logo' );

		$html = '<input type="checkbox" id="login_logo" name="ewl_display_options[login_logo]" value="1" ' . checked( 1, isset( $options['login_logo'] ) ? $options['login_logo'] : 0, false ) . '/>';
		$html .= '<label for="login_logo">&nbsp;'  . $args[0] . '</label>';

		echo $html;

	} // end toggle_login_logo_callback






	public function validate_input_examples( $input ) {

		// Create our array for storing the validated options
		$output = array();

		// Loop through each of the incoming options
		foreach( $input as $key => $value ) {

			// Check to see if   the current option has a value. if   so, process it.
			if ( isset( $input[$key] ) ) {

				// Strip all HTML and PHP tags and properly handle quoted strings
				$output[$key] = strip_tags( stripslashes( $input[ $key ] ) );

			} // end if  

		} // end foreach

		// Return the array processing any additional functions filtered by this action
		return apply_filters( 'validate_input_examples', $output, $input );

	} // end validate_input_examples




}