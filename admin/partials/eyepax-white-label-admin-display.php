<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       eyepax.com
 * @since      1.0.0
 *
 * @package    Eyepax_White_Label
 * @subpackage Eyepax_White_Label/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->


