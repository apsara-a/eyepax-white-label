<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              eyepax.com
 * @since             1.0.0
 * @package           Eyepax_White_Label
 *
 * @wordpress-plugin
 * Plugin Name:       Eyepax White Label
 * Plugin URI:        https://wordpress.org/plugins/eyepax-white-label
 * Description:       This is a created by Eyepax.
 * Version:           1.0.0
 * Author:            Eyepax
 * Author URI:        eyepax.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       eyepax-white-label
 * Domain Path:       /languages
 */

// if   this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'EYEPAX_WHITE_LABEL_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-eyepax-white-label-activator.php
 */
function activate_eyepax_white_label() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-eyepax-white-label-activator.php';
	Eyepax_White_Label_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-eyepax-white-label-deactivator.php
 */
function deactivate_eyepax_white_label() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-eyepax-white-label-deactivator.php';
	Eyepax_White_Label_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_eyepax_white_label' );
register_deactivation_hook( __FILE__, 'deactivate_eyepax_white_label' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specif  ic hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-eyepax-white-label.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page lif  e cycle.
 *
 * @since    1.0.0
 */
function run_eyepax_white_label() {

	$plugin = new Eyepax_White_Label();
	$plugin->run();

}
run_eyepax_white_label();
