<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       eyepax.com
 * @since      1.0.0
 *
 * @package    Eyepax_White_Label
 * @subpackage Eyepax_White_Label/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specif  ic hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identif  ier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Eyepax_White_Label
 * @subpackage Eyepax_White_Label/includes
 * @author     Eyepax <info@eyepax.com>
 */
class Eyepax_White_Label {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Eyepax_White_Label_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identif  ier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identif  y this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if   ( defined( 'EYEPAX_WHITE_LABEL_VERSION' ) ) {
			$this->version = EYEPAX_WHITE_LABEL_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'eyepax-white-label';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Eyepax_White_Label_Loader. Orchestrates the hooks of the plugin.
	 * - Eyepax_White_Label_i18n. Defines internationalization functionality.
	 * - Eyepax_White_Label_Admin. Defines all hooks for the admin area.
	 * - Eyepax_White_Label_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-eyepax-white-label-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-eyepax-white-label-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-eyepax-white-label-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-eyepax-white-label-public.php';

		$this->loader = new Eyepax_White_Label_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Eyepax_White_Label_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Eyepax_White_Label_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$options1 = get_option('ewl_display_options');

		$plugin_admin = new Eyepax_White_Label_Admin( $this->get_plugin_name(), $this->get_version() );
		$plugin_settings = new Eyepax_White_Label_Admin_Settings( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		
		$this->loader->add_action( 'admin_menu', $plugin_settings, 'Setup_Plugin_Options_Menu' );
		$this->loader->add_action( 'admin_init', $plugin_settings, 'initialize_display_options' );
		// $this->loader->add_action( 'admin_init', $plugin_settings, 'initialize_social_options' );
		// $this->loader->add_action( 'admin_init', $plugin_settings, 'initialize_input_examples' );
		
		if (!empty($options1['login_username'])) {
			$this->loader->add_filter( 'authenticate', $plugin_admin, 'remove_email_login' );
			$this->loader->add_filter( 'gettext', $plugin_admin, 'register_text' );
			$this->loader->add_filter( 'ngettext', $plugin_admin, 'register_text' );
		}

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$options1 = get_option('ewl_display_options');

		$plugin_public = new Eyepax_White_Label_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		// $this->loader->add_action( 'login_enqueue_scripts', $plugin_public, 'wpb_login_logo' );
		
		$this->loader->add_filter( 'login_headerurl', $plugin_public, 'Ewl_Login_Logo_url' );
		$this->loader->add_filter( 'login_headertitle', $plugin_public, 'Ewl_Login_Logo_Url_title' );
		$this->loader->add_action( 'login_head', $plugin_public, 'Ewl_Custom_Login_style' );
		
		$this->loader->add_filter( 'login_footer', $plugin_public, 'Ewl_Custom_Login_head' );
		$this->loader->add_filter( 'login_footer', $plugin_public, 'Ewl_Copy_rights' );

		if (!empty($options1['login_logo'])) {
			$this->loader->add_filter( 'login_headerurl', $plugin_public, 'Ewl_Custom_logo' );
			$this->loader->add_action( 'after_setup_theme', $plugin_public, 'Ewl_Config_Custom_logo' );
		}

		if (!empty($options1['hide_lostpassword'])) {
			$this->loader->add_filter( 'login_footer', $plugin_public, 'Ewl_Hide_Lost_Your_password' );
		}
		
		if (!empty($options1['back_to_blog'])) {
			$this->loader->add_filter( 'login_footer', $plugin_public, 'Ewl_Back_To_blog' );
		}	
		if (!empty($options1['custom_err_msg'])) {
			$this->loader->add_filter( 'login_errors', $plugin_public, 'Ewl_Custom_Login_Error_message' );

		}

		
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identif  y it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Eyepax_White_Label_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
