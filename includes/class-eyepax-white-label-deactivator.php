<?php

/**
 * Fired during plugin deactivation
 *
 * @link       eyepax.com
 * @since      1.0.0
 *
 * @package    Eyepax_White_Label
 * @subpackage Eyepax_White_Label/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Eyepax_White_Label
 * @subpackage Eyepax_White_Label/includes
 * @author     Eyepax <info@eyepax.com>
 */
class Eyepax_White_Label_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
